package com.horman.lab2cloud.service

import com.horman.lab2cloud.dao.FarmFieldDao
import com.horman.lab2cloud.api.field.dto.FarmField
import com.horman.lab2cloud.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class FarmFieldService(private val farmFieldDao: FarmFieldDao) {
    suspend fun getFieldById(id: Int): FarmField {
        return farmFieldDao.getFieldById(id) ?: throw RecordNotFound()
    }

    suspend fun getFieldsByFarmId(farmId: String): List<FarmField> {
        return farmFieldDao.getFieldsByFarmId(farmId)
    }

    suspend fun getFieldsIdByFarmId(farmId: String): List<Int> {
        return farmFieldDao.getFieldsIdsByFarmId(farmId)
    }
}
