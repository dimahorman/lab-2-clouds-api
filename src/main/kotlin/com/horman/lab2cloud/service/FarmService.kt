package com.horman.lab2cloud.service

import com.horman.lab2cloud.dao.FarmDao
import com.horman.lab2cloud.api.farm.dto.Farm
import com.horman.lab2cloud.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class FarmService(private val farmDao: FarmDao) {
    suspend fun getFarmsByFarmerName(farmerName: String): List<Farm> {
        return farmDao.getFarmsByFarmerName(farmerName)
    }

    suspend fun getFarmById(id: String): Farm {
        return farmDao.getFarmById(id) ?: throw RecordNotFound()
    }

    suspend fun getFarmsIdByFarmerName(farmerName: String): List<String> {
        return farmDao.getFarmsIdByFarmerName(farmerName)
    }

}
