package com.horman.lab2cloud.service

import com.horman.lab2cloud.dao.SensorDao
import com.horman.lab2cloud.api.sensor.dto.Sensor
import com.horman.lab2cloud.api.sensor.dto.SensorType
import com.horman.lab2cloud.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class SensorService(private val sensorDao: SensorDao) {
    suspend fun getSensor(id: Int): Sensor {
        return this.sensorDao.getSensor(id) ?: throw RecordNotFound()
    }

    suspend fun getSensorsByFarmId(farmId: String): List<Sensor> {
        return this.sensorDao.getSensorsByFarmId(farmId)
    }

    suspend fun getSensorsByFieldId(fieldId: Int): List<Sensor> {
        return this.sensorDao.getSensorsByFieldId(fieldId)
    }

    suspend fun getSensorsIdByFieldId(fieldId: Int): List<Int> {
        return this.sensorDao.getSensorsIdByFieldId(fieldId)
    }

    suspend fun setSensorValue(sensorId: Int, value: String) {
        this.sensorDao.updateSensorValue(sensorId, "$value%")
    }
}
