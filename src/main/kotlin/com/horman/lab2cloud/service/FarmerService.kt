package com.horman.lab2cloud.service

import com.horman.lab2cloud.api.farmer.dto.Farmer
import com.horman.lab2cloud.dao.FarmerDao
import com.horman.lab2cloud.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class FarmerService(private val farmerDao: FarmerDao) {
    suspend fun getFarmer(name: String): Farmer {
        return farmerDao.getFarmer(name) ?: throw RecordNotFound()
    }

    suspend fun getFarmers(): List<Farmer> {
        return farmerDao.getFarmers()
    }

    suspend fun getFarmersNames(): List<String> {
        return farmerDao.getFarmersNames()
    }
}
