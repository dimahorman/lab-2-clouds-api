package com.horman.lab2cloud.dao

import com.horman.lab2cloud.api.field.dto.FarmField
import com.horman.lab2cloud.api.field.dto.FarmFieldType
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.stereotype.Repository

@Repository
class FarmFieldDao(@Qualifier("pg_client") private val databaseClient: DatabaseClient) {
    suspend fun getFieldsByFarmId(farmId: String): List<FarmField> {
        return databaseClient.execute("select id, farm_id, type::text from farm_fields where farm_id = $1")
                .bind(0, farmId)
                .fetch()
                .all()
                .map {
                    FarmField(
                            it["id"] as Int,
                            it["farm_id"] as String,
                            FarmFieldType.valueOf(it["type"] as String)
                    )
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun getFieldsIdsByFarmId(farmId: String): List<Int> {
        return databaseClient.execute("select id from farm_fields where farm_id = $1")
                .bind(0, farmId)
                .fetch()
                .all()
                .map {
                    it["id"] as Int
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun getFieldById(id: Int): FarmField? {
        return databaseClient.execute("select id, farm_id, type::text from farm_fields where id = $1")
                .bind(0, id)
                .fetch()
                .one()
                .map {
                    FarmField(
                            it["id"] as Int,
                            it["farm_id"] as String,
                            FarmFieldType.valueOf(it["type"] as String)
                    )
                }
                .awaitFirstOrNull()
    }
}
