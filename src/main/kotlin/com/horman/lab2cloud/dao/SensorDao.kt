package com.horman.lab2cloud.dao

import com.horman.lab2cloud.api.sensor.dto.Sensor
import com.horman.lab2cloud.api.sensor.dto.SensorType
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.stereotype.Repository

@Repository
class SensorDao(@Qualifier("pg_client") private val databaseClient: DatabaseClient) {
    suspend fun getSensor(id: Int): Sensor? {
        return databaseClient.execute("select id, field_id, gps_cords, measured_value, type::text from sensors where id = $1")
                .bind(0, id)
                .fetch()
                .one()
                .map {
                    Sensor(
                            id,
                            it["field_id"] as Int,
                            it["gps_cords"] as String,
                            it["measured_value"] as String,
                            SensorType.valueOf(it["type"] as String)
                    )
                }
                .awaitFirstOrNull()
    }

    suspend fun getSensorsByFarmId(farmId: String): List<Sensor> {
        return databaseClient.execute("select id, field_id, gps_cords, measured_value, type::text from sensors where field_id in (select id from farm_fields where farm_id = $1)")
                .bind(0, farmId)
                .fetch()
                .all()
                .map {
                    Sensor(
                            it["id"] as Int,
                            it["field_id"] as Int,
                            it["gps_cords"] as String,
                            it["measured_value"] as String,
                            SensorType.valueOf(it["type"] as String)
                    )
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun getSensorsByFieldId(fieldId: Int): List<Sensor> {
        return databaseClient.execute("select id, field_id, gps_cords, measured_value, type::text from sensors where field_id = $1")
                .bind(0, fieldId)
                .fetch()
                .all()
                .map {
                    Sensor(
                            it["id"] as Int,
                            it["field_id"] as Int,
                            it["gps_cords"] as String,
                            it["measured_value"] as String,
                            SensorType.valueOf(it["type"] as String)
                    )
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun getSensorsIdByFieldId(fieldId: Int): List<Int> {
        return databaseClient.execute("select id from sensors where field_id = $1")
                .bind(0, fieldId)
                .fetch()
                .all()
                .map {
                    it["id"] as Int
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun updateSensorValue(id: Int, value: String) {
        databaseClient.execute("update sensors set measured_value = $2 where id = $1")
            .bind(0, id)
            .bind(1, value)
            .then()
            .awaitFirstOrNull()
    }
}
