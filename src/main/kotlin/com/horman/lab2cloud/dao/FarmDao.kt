package com.horman.lab2cloud.dao

import com.horman.lab2cloud.api.farm.dto.Farm
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.stereotype.Repository

@Repository
class FarmDao(@Qualifier("pg_client") private val databaseClient: DatabaseClient) {
    suspend fun getFarmsByFarmerName(farmerName: String): List<Farm> {
        return databaseClient.execute("select * from farms where owner = $1")
                .bind(0, farmerName)
                .fetch()
                .all()
                .map {
                    Farm(
                            it["id"] as String,
                            it["owner"] as String
                    )
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun getFarmsIdByFarmerName(farmerName: String): List<String> {
        return databaseClient.execute("select id from farms where owner = $1")
                .bind(0, farmerName)
                .fetch()
                .all()
                .map {
                    it["id"] as String
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun getFarmById(id: String): Farm? {
        return databaseClient.execute("select * from farms where id = $1")
                .bind(0, id)
                .fetch()
                .one()
                .map {
                    Farm(
                            it["id"] as String,
                            it["owner"] as String
                    )
                }
                .awaitFirstOrNull()
    }
}
