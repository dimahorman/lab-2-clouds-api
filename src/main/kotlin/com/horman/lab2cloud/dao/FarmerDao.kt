package com.horman.lab2cloud.dao

import com.horman.lab2cloud.api.farmer.dto.Farmer
import com.horman.lab2cloud.api.farmer.dto.SkillLevel
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.stereotype.Repository

@Repository
class FarmerDao(@Qualifier("pg_client") private val databaseClient: DatabaseClient) {
    suspend fun getFarmers(): List<Farmer> {
        return databaseClient.execute("select skill_level::text, name, age, birth_city from farmers")
                .fetch()
                .all()
                .map {
                    Farmer(
                            it["name"] as String,
                            it["age"] as Int,
                            it["birth_city"] as String,
                            SkillLevel.valueOf(it["skill_level"] as String)
                    )
                }
                .collectList()
                .awaitFirst()
    }

    suspend fun getFarmersNames(): List<String> {
        return databaseClient.execute("select name from farmers")
                .fetch()
                .all()
                .map {
                    it["name"] as String
                }
                .collectList()
                .awaitFirst()
    }


    suspend fun getFarmer(name: String): Farmer? {
        return databaseClient.execute("select skill_level::text, name, age, birth_city from farmers where name = $1")
                .bind(0, name)
                .fetch()
                .one()
                .map {
                    Farmer(
                            it["name"] as String,
                            it["age"] as Int,
                            it["birth_city"] as String,
                            SkillLevel.valueOf(it["skill_level"] as String)
                    )
                }
                .awaitFirstOrNull()
    }
}
