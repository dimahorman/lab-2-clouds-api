package com.horman.lab2cloud.api.farm.dto

data class Farm(
        val id: String,
        val owner: String
)
