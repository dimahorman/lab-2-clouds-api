package com.horman.lab2cloud.api.farm

import com.horman.lab2cloud.api.farm.dto.Farm
import com.horman.lab2cloud.api.farmer.dto.Farmer
import com.horman.lab2cloud.api.field.dto.FarmField
import com.horman.lab2cloud.service.FarmFieldService
import com.horman.lab2cloud.service.FarmService
import com.horman.lab2cloud.service.FarmerService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class FarmController(
        private val farmFieldService: FarmFieldService,
        private val farmService: FarmService,
        private val farmerService: FarmerService
) {

    @GetMapping("/farmers")
    suspend fun getFarmers(): List<Farmer> {
        return farmerService.getFarmers()
    }

    @GetMapping("/farmers/names")
    suspend fun getFarmersNames(): List<String> {
        return farmerService.getFarmersNames()
    }

    @GetMapping("/farmers/{name}")
    suspend fun getFarmer(@PathVariable name: String): Farmer {
        return farmerService.getFarmer(name)
    }

    @GetMapping("/farms/{id}")
    suspend fun getFarm(@PathVariable id: String): Farm {
        return farmService.getFarmById(id)
    }

    @GetMapping("/farmers/{farmerName}/farms")
    suspend fun getFarmsByFarmerName(@PathVariable farmerName: String): List<Farm> {
        return farmService.getFarmsByFarmerName(farmerName)
    }

    @GetMapping("/farmers/{farmerName}/farms/ids")
    suspend fun getFarmsIdsByFarmerName(@PathVariable farmerName: String): List<String> {
        return farmService.getFarmsIdByFarmerName(farmerName)
    }

    @GetMapping("/fields/{id}")
    suspend fun getFarmField(@PathVariable id: Int): FarmField {
        return farmFieldService.getFieldById(id)
    }

    @GetMapping("/farms/{farmId}/fields")
    suspend fun getFarmFieldsByFarmId(@PathVariable farmId: String): List<FarmField> {
        return farmFieldService.getFieldsByFarmId(farmId)
    }

    @GetMapping("/farms/{farmId}/fields/ids")
    suspend fun getFarmFieldsIdsByFarmId(@PathVariable farmId: String): List<Int> {
        return farmFieldService.getFieldsIdByFarmId(farmId)
    }
}
