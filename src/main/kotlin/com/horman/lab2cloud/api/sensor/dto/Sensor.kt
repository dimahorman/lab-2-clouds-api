package com.horman.lab2cloud.api.sensor.dto

data class Sensor(
        val id: Int,
        val fieldId: Int,
        val gpsCoordinates: String,
        val measuredValue: String,
        val type: SensorType
)

enum class SensorType {
    HUMIDITY,
    LIGHTING,
    NONE
}
