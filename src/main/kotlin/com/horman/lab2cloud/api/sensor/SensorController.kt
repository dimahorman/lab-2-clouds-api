package com.horman.lab2cloud.api.sensor

import com.horman.lab2cloud.api.sensor.dto.Sensor
import com.horman.lab2cloud.service.SensorService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api")
class SensorController(val sensorService: SensorService) {
    @GetMapping("/sensors/{id}")
    suspend fun getSensor(@PathVariable id: Int): Sensor {
        return sensorService.getSensor(id)
    }

    @GetMapping("/farms/{farmId}/sensors")
    suspend fun getSensorsByFarmId(@PathVariable farmId: String): List<Sensor> {
        return sensorService.getSensorsByFarmId(farmId)
    }

    @GetMapping("/fields/{fieldId}/sensors")
    suspend fun getSensorsByFieldId(@PathVariable fieldId: Int): List<Sensor> {
        return sensorService.getSensorsByFieldId(fieldId)
    }

    @GetMapping("/fields/{fieldId}/sensors/ids")
    suspend fun getSensorsIdsByFieldId(@PathVariable fieldId: Int): List<Int> {
        return sensorService.getSensorsIdByFieldId(fieldId)
    }

    @PatchMapping("/sensors/{id}")
    suspend fun setSensorValue(@PathVariable id: Int, @RequestParam value: String) {
        sensorService.setSensorValue(id, value)
    }
}
