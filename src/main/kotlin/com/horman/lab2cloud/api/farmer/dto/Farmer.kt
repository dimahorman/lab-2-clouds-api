package com.horman.lab2cloud.api.farmer.dto

data class Farmer(
        val name: String,
        val age: Int,
        val birthCity: String,
        val skillLevel: SkillLevel
)

enum class SkillLevel {
    HIGH,
    AVERAGE,
    LOW
}

