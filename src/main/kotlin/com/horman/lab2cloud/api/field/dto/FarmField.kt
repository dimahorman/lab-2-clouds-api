package com.horman.lab2cloud.api.field.dto

data class FarmField(
        val id: Int,
        val farmId: String,
        val type: FarmFieldType
)

enum class FarmFieldType {
    BANANA,
    CUCUMBER,
    POTATO,
    CORN,
    NONE
}
